//======================================================
// Main program routine
// - Device name  : MC96F8104
// - Package type : 8SOP
//======================================================
// For XDATA variable : V1.041.00 ~
#define		MAIN	1

// Generated    : Tue, Jul 26, 2016 (15:38:21)

#include	"MC96F8204.h"
#include	"func_def.h"
#include "Ram_def.h"
//======================================================
// Function and global variables definition
//======================================================
void system_initial(void);
void sp_DipSwitch_Read(void);
IDATA unsigned char b_SW_OPTION;


IDATA bit bMWReadOk;
IDATA bit bPWMOffPlay;
IDATA bit bPWMSet;

unsigned char ADC_read(unsigned char nADC_sel)	// ADC 포트 입력
{
	unsigned char nData;

	// initialize A/D convertor
	ADCCRL = 0x80;  		// setting
	ADCCRH = 0x00;  	// trigger source, alignment, frequency
	
	ADCCRL = (ADCCRL & 0xf0) | (nADC_sel & 0x0f); // select channel
	ADCCRL |= 0x40; 	// start ADC

	while(!(ADCCRL & 0x10));	// wait ADC busy
	nData = ADCDRH ;	// read ADC
	return	nData;
}
void MWS_sensor_read(void)
{
	if(nMWReadTimer >= TIME_1MSEC)	// 1ms 마다 PIR SENSOR Read
	{
		nMWReadTimer = 0;

		nAdcMWRead = ADC_read(ADC_07);	// PIR SENSOR Read
		nAdcMWSum += nAdcMWRead;

		nAdcMWCount++;
		if(nAdcMWCount >= 10)
		{
			nAdcMWCount = 0;

			nMWSensorReadValue = nAdcMWSum/10;	// PIR SENSOR Read
			nAdcMWSum = 0;
			bMWReadOk = SET;
		}
	}
}
IDATA unsigned char g_gain;
void Func_process(void)
{
	unsigned int	g_gain_Low, g_gain_High;
	unsigned char	g_count;

	if(b_LightoffCnt == 1)
		bPWMOffPlay = SET;
	
	if (b_AD_ScanLockTime != 0)
	{
		bMWReadOk = 0;
		b_Sensor_ChkTime = TIME_50MSEC;
		b_SensorInCount = 0;
		return;
	}
	if(bMWReadOk == SET)
	{
		bMWReadOk = CLR;
		g_SensorValue = nMWSensorReadValue;
		if (stSetOptionSensitivity == 0)
		{
			//g_gain_Low = 66;			//1.3v
			//g_gain_High = 138;		//2.7v

			g_gain_Low = 10;			//1.3v
			g_gain_High = 200;		//2.7v
			g_count = 2;
		}
		if ((g_SensorValue <  g_gain_Low) || (g_SensorValue > g_gain_High))
		{
			b_SensorInCount++;
		}
	}
	if(b_Sensor_ChkTime == 0)
	{
		if(b_SensorInCount >= g_count)
		{
			if(b_Status !=  ON)
			{
				if (b_Test_cnt == 0)
				{
					bPWMOffPlay = CLR;
					bPWMSet = CLR;
					P02 = 1;
				}
				b_Test_cnt = 20000;//0000;
	
				b_Status  = OFF;
				b_AD_ScanLockTime = TIME_300MSEC;
			}
		}
		else 
		{
			if (b_Status != OFF)
			{
				b_Status = ON;
				b_AD_ScanLockTime = TIME_300MSEC;
				P02 = 0;
			}
		}
		b_Sensor_ChkTime = TIME_50MSEC;
		b_SensorInCount = 0;
	}
}
 idata  unsigned char iiii = 0;
void main()
{
	system_initial();
	iiii = 0xff;
	while(1)
	{
	//	WDTCR |= 0x20;		// Clear Watch-dog timer
		sp_DipSwitch_Read();

		MWS_sensor_read();
		Func_process();

	}
}



//======================================================
// peripheral setting routines
//======================================================


#if 0

void Timer1_init()
{
	// initialize Timer1
	// 16bit PWM, period = 1.000000mS ( 1000.000000Hz )
	//     PWM duty = 50.000000%
	T1CRH = 0x20;   	// PWM setting High, 1 shot mode
	//T1CRH |= 0x30;	// PWM repeat mode
	T1CRL = 0xC0;   	// PWM setting Low
	T1BDRH = 0x0F;  	// duty High
	T1BDRL = 0x9F;  	// duty Low
	T1ADRH = 0x1F;  	// period count High
	T1ADRL = 0x3F;  	// period count Low
	//T1CRL |= 0x08;	// if this bit is 1, reload is disabled
	T1CRH |= 0x80;  	// enable counter
}
#endif


#define PT_KEY1_I ((P06 == 1) ? 1 : 0)


void system_initial(void)
{
	cli();
	// initialize ports
	//   3 : P06      in  SWITCH
	//   4 : AN7      in  MWSensor_IN
	//   7 : PWM1o    out PWM_OUT
	P0IO = 0x3D;    	// direction
	P0PU = 0x00;    	// pullup
	P0OD = 0x00;    	// open drain
	P0DB = 0x00;    	// bit7~6(debounce clock), bit4~0=P04~00 debounce
	P0   = 0x00;    	// port initial value

	P1IO = 0xFF;    	// direction
	P1PU = 0x00;    	// pullup
	P1OD = 0x00;    	// open drain
	P1   = 0x00;    	// port initial value

	P2IO = 0xFF;    	// direction
	P2PU = 0x00;    	// pullup
	P2OD = 0x00;    	// open drain
	P2   = 0x00;    	// port initial value

	// Set port functions
	P0FSRH = 0x04;  	// P0 selection High
	P0FSRM = 0x00;  	// P0 selection Middle
	P0FSRL = 0x00;  	// P0 selection Low			//todo 0x20 pwm
	P1FSR = 0x00;   	// P1 selection

	// internal RC clock (8.0MHz)
	OSCCR = 0x20;   	// Set Int. OSC
	SCCR = 0x00;    	// Use Int. OSC


	// initialize Timer0
	// 8bit timer, period = 1.000000mS
	T0CR = 0x86;		// timer setting
	T0DR = 0xF9;		// period count
	IE2 |= 0x02;		// Enable Timer0 interrupt
	T0CR |= 0x01;		// clear counter
	
#if 0
	// initialize Watch-dog timer
	WDTDR = 0x01;		// period
	WDTCR = 0x80;		// setting
	WDTCR |= 0x02;	// Use WDTRC
	IE3 |= 0x08;		// Enable WDT interrupt
#endif
	sei();				// enable INT.
}
void sp_DipSwitch_Read(void)
{
	unsigned char Now_Value, Now_Value_prev;
	unsigned char	SamplingCount=0, max_cnt=0;

	do{
		Now_Value = 0;

	   	if (PT_KEY1_I == 1) Now_Value |= 0x01;


		if (Now_Value != Now_Value_prev)
		{
			Now_Value_prev = Now_Value;
			SamplingCount = 0;
		}
     		max_cnt++;
		if (max_cnt > 20) break;
		SamplingCount++;

     	}while(SamplingCount < 10);

	//if (Now_Value >= 3) Now_Value = 0;	//!!!!!!!!
	
	b_SW_OPTION = Now_Value;
}

