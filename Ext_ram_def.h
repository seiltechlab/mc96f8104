//=================================================================
//     extern ram Define
//================================================================= 
#include "Variable.h"				//

// user PAGE0 RAM define : 192-16(System Area) = 176Bytes

extern IDATA unsigned char nSensitiveReadValue;
extern IDATA unsigned char nMWSensorReadValue;
extern IDATA unsigned char nCdsReadValue;
extern IDATA unsigned char nREFValue;
extern IDATA unsigned char nREFTemp;

extern IDATA unsigned char nPirSenReadCnt;

extern IDATA unsigned int nPWM_DutyVal;
extern IDATA unsigned char nPWM_Period;
extern IDATA unsigned char nPWM_Duty;
extern IDATA unsigned char nPWM_Tmp;


extern IDATA unsigned char nAdcMWCount;
extern IDATA unsigned char nAdcReadCount;
extern IDATA unsigned char nAdcREFCount;
extern IDATA unsigned int nAdcREFRead;
extern IDATA unsigned int nAdcMWRead;
extern IDATA unsigned int nAdcTimeRead;
extern IDATA unsigned int nAdcCdsRead;
extern IDATA unsigned int nAdcREFSum;
extern IDATA unsigned int nAdcMWSum;
extern IDATA unsigned int nAdcTimeSum;
extern IDATA unsigned int nAdcCdsSum;

extern IDATA unsigned int nOUT2OnTime;
extern IDATA unsigned int nOUT3OnTime;
extern IDATA unsigned int nOUT4OnTime;
extern IDATA unsigned int nOUT8OnTime;
extern IDATA unsigned int nLAMPOnTime;

extern IDATA unsigned char nMWReadTimer;
extern IDATA unsigned int nOUTPUTTimer;

extern IDATA unsigned int n1SecTimer;
extern IDATA unsigned char n1MinTimer;
extern IDATA unsigned char n30MinTimer;
extern IDATA unsigned char nMinTimer;
extern IDATA unsigned char n5MinTimer;

extern IDATA unsigned int nPWMTimer;

extern IDATA unsigned int 	b_Test_cnt;
extern IDATA unsigned int 	AD_Temp_data_16;
extern IDATA unsigned int 	b_AD_ScanLockTime;
extern IDATA unsigned int 	b_LightoffCnt;

extern IDATA unsigned char	b_Led_Status;
extern IDATA unsigned char	b_PT_BRIGHT_OPTION;
extern IDATA unsigned char	b_Sensor_ChkTime;
extern IDATA unsigned char	b_SensorInCount;
extern IDATA unsigned char	stSetOptionSensitivity;
extern IDATA unsigned char	g_SensorValue;
extern IDATA unsigned char	b_Status;



//=================================================================


