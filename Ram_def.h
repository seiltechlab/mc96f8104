
//=================================================================
//     User Ram Define
//================================================================= 
#include "Variable.h"				//

// user PAGE0 RAM define : 192-16(System Area) = 176Bytes

IDATA unsigned char nSensitiveReadValue;
IDATA unsigned char nMWSensorReadValue;
IDATA unsigned char nCdsReadValue;
IDATA unsigned char nREFValue;
IDATA unsigned char nREFTemp;

IDATA unsigned char nPirSenReadCnt;

IDATA unsigned int nPWM_DutyVal;
IDATA unsigned char nPWM_Period;
IDATA unsigned char nPWM_Duty;
IDATA unsigned char nPWM_Tmp;

// ADC def
IDATA unsigned char nAdcMWCount;
IDATA  unsigned char nAdcReadCount;
IDATA unsigned char nAdcREFCount;
IDATA  unsigned int nAdcREFRead;
IDATA unsigned int nAdcMWRead;
IDATA unsigned int nAdcTimeRead;
IDATA unsigned int nAdcCdsRead;
IDATA unsigned int nAdcREFSum;
IDATA unsigned int nAdcMWSum;
IDATA unsigned int nAdcTimeSum;
IDATA unsigned int nAdcCdsSum;

//Timer def
IDATA unsigned int nOUT2OnTime;
IDATA unsigned int nOUT3OnTime;
IDATA unsigned int nOUT4OnTime;
IDATA unsigned int nOUT8OnTime;
IDATA unsigned int nLAMPOnTime;
IDATA unsigned char nMWReadTimer;
IDATA unsigned int nOUTPUTTimer;
IDATA unsigned int n1SecTimer;
IDATA unsigned char n1MinTimer;
IDATA unsigned char n30MinTimer;
IDATA unsigned char nMinTimer;
IDATA unsigned char n5MinTimer;
IDATA unsigned int nPWMTimer;


IDATA unsigned int		AD_Temp_data_16;
IDATA unsigned char	b_Sensor_ChkTime;
IDATA unsigned char	b_SensorInCount;
IDATA unsigned int		b_AD_ScanLockTime;
IDATA unsigned char	b_Led_Status;
IDATA unsigned char	b_PT_BRIGHT_OPTION;

IDATA unsigned char	stSetOptionSensitivity;
IDATA unsigned char	g_SensorValue;
IDATA unsigned char	b_Status;
IDATA unsigned int		b_LightoffCnt;
IDATA unsigned int		b_Test_cnt;

//=================================================================

