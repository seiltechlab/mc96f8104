#include "ext_ram_def.h"
#include	"MC96F8204.h"
//======================================================
// interrupt routines
//======================================================

void INT_WDT() interrupt 21
{
	// Watch-dog timer interrupt
}
void INT_Timer0() interrupt 13
{
	if(b_AD_ScanLockTime)	b_AD_ScanLockTime--;
	if(b_Sensor_ChkTime)		b_Sensor_ChkTime--;
	if(b_Test_cnt)			b_Test_cnt--;
	if (b_LightoffCnt>0)
		b_LightoffCnt--;
	nMWReadTimer++;
	nPWMTimer++;

	n1SecTimer++;
	if(n1SecTimer >= TIME_1SEC)
	{
		n1SecTimer = 0;
			n1MinTimer++;
		if(n1MinTimer  >= 60)
		{
			n1MinTimer = 0;
			n30MinTimer++;
		}
	}

	nOUTPUTTimer++;
	if(nOUTPUTTimer >= TIME_1SEC)
	{

		nOUTPUTTimer = 0;
		nMinTimer++;
		if(nMinTimer >= 60)
		{
			nMinTimer = 0;
			n5MinTimer++;
		}
	}	
}

