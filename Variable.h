//=================================================================

//     Define

//================================================================= 

#define	TRUE				1

#define	FALSE				0

#define  	ON					1			

#define  	OFF					0	

#define	SET					1

#define	CLR					0

#define	HIGH				1

#define	LOW				0
#define  	IDATA			idata

#define	TIME_0MSEC				0
#define	TIME_1MSEC				1
#define	TIME_2MSEC				2
#define	TIME_3MSEC				3
#define	TIME_5MSEC				5
#define	TIME_10MSEC			10
#define	TIME_50MSEC			50
#define	TIME_100MSEC			100
#define	TIME_200MSEC			200
#define	TIME_300MSEC			300
#define	TIME_500MSEC			500
#define	TIME_1SEC				1000
#define	TIME_2SEC				2000
#define	TIME_3SEC				3000
#define	TIME_4SEC				4000
#define	TIME_5SEC				5000
#define	TIME_10SEC				10000
#define	TIME_30SEC				30000


//adc channel


#define	ADC_00			0x00
#define	ADC_01			0x01
#define	ADC_02			0x02
#define	ADC_03			0x03
#define	ADC_04			0x04
#define	ADC_07			0x07

